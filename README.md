# USGS Water Services Data Python Package

The usgs_waterservices_data package allows users to fetch data from the USGS Water Services Daily Services REST API and return a dataframe containing location, dates, and data.


## To Do
* Addition of additional possible filters for datasets. Includes but not limited to - Return most recent values only, Return all values from a period from now, statistic codes, sit status, sites with these site types, minimum site altitude,  maximum site altitude, surface water attributes, ground water attributes
* Addition of error handling
* Testing additions
* Additon of instant value service
* Addition of site service
* Addition of water quality services
* Addition of attribute that holds all site information
* Add a % complete updating display when pulling data and converting to dataframe


## Known Issues
* In initial use, it was determined that the API does not always return all possible stations. When specifying the exact station number, data will be returned, but when using the geo bound box or state code the stations will not appear. it is unclear as to why this is occuring, but it is on the USGS side unfortunately.
