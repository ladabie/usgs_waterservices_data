import requests
import pandas
import re
import sys


class WaterStationDailyData():
    """This is a class for Water Station Data from the USGS Water API for daily
    values of data at various water stations across USA
    (https://waterservices.usgs.gov/rest/DV-Service.html).

    Attributes
    ----------
    station_numbers : type
        Description of parameter `station_numbers`.
    geographic_boundary_box : type
        Description of parameter `geographic_boundary_box`.
    date_range : type
        Date range for data to be pulled for. Should be in list format
        with start_date followed by end date. Date format should abide
        by ISO-8601 format (YYYY-MM-DD). Hours. minutes, seconds and
        timezone values are not allowed.
    """
    def __init__(self,date_range,station_numbers=None,geo_boundary_box=None
                 ,us_state=None,us_counties=None, site_type=None
                 ,metric_codes=['00060','00065']):
        self.station_numbers = station_numbers
        self.geo_boundary_box = geo_boundary_box
        self.us_state = us_state
        self.us_counties = us_counties
        self.date_range = date_range
        self.site_type= site_type
        self.metric_codes=metric_codes


    def get_data_return_dataframe(self):
        """ A  function to execute building url, fetching data, and
        returns a dataframe with all data.
        """

        self.build_api_url()
        self.fetch_data()
        self.convert_json_to_dataframes()
        print("Returning Dataframe")
        return self.water_dataframe

    def get_data_return_class(self):
        """ A  function to execute building url, fetching data, and
        returns a dataframe with all data.
        """

        self.build_api_url()
        self.fetch_data()
        self.convert_json_to_dataframes()
        print("Returning Class")
        return self

    def get_data_return_csv(self):
        """ A function to execute building url, fetching  data,
        and creates two csvs title station_information.csv
        and water_data.csv in folder executed in.

        Station information contains station_number,
        name, latitude, longitude,  state_abbr, state,
        watershed_code, county_code, county_name.

        water_data contains comma separated data by station_number,
        date, metric, metric_units, value.
        """

        self.build_api_url()
        self.fetch_data()
        self.convert_json_to_csv()
        return self

    def build_api_url(self):
        """ A function to build the API url for fetching the USGS water
        station data from the USGS Water Services API for Daily Values.
        """
        base_url = "https://waterservices.usgs.gov/nwis/dv/?format=json"
        self.__create_major_filter()
        self.__parse_start_end_dates()
        self.__create_site_type_filter()
        self.__create_metric_filter()
        self.complete_url = (base_url
                        + self.major_filter
                        + self.start_date + self.end_date
                        + self.site_type_filter
                        + self.metric_filter
                        )


    def fetch_data(self):
        """ A function to fetch data using the complete url to return
        json from USGS water services API for daily values.

        """
        print("Fetching Data from USGS Water Services API")
        self.response = requests.get(self.complete_url, timeout=None)
        self.response.raise_for_status()


    def convert_json_to_dataframes(self):
        """ A function to convert json with different datasets
        and combine data into dataframes. Returns a dictionary
        of dataframes.

        """
        print("Converting Data into Dataframe.")
        json_data = self.response.json()
        complete_df = pandas.DataFrame()
        self.station_dictionary={}
        self.stations_returned=[]
        self.empty_load=[]
        self.update_attempts=[]
        self.merge_attempts=[]
        self.append_attempts=[]
        self.total_attempts=[]
        self.no_data=[]
        for data in json_data['value']['timeSeries']:
            location = data['sourceInfo']['siteCode'][0]['value']
            metric = data['variable']['variableName'].split(',')[0].lower()
            units= data['variable']['unit']['unitCode'].lower()
            statistic = data['variable']['options']['option'][0]['value'].lower()
            self.__create_station_dictionary(data)
            self.total_attempts.append(location)
            temp_data_load=pandas.DataFrame(data['values'][0]['value'])
            if 'qualifiers' in temp_data_load.columns:
                temp_data_load.drop(columns='qualifiers', inplace=True)

            column_name='{stat} {metric} ({units})'.format(stat=statistic
                                                     ,metric=metric
                                                     ,units=units)


            if 'value' not in temp_data_load.columns:
                self.no_data.append(location)
                self.__raise_stations_with_no_data(location,metric)
                pass
            else:
                temp_data_load.rename(columns={'dateTime':'date'
                                               ,'value': column_name}
                                      ,inplace=True)
                temp_data_load['date'] = pandas.to_datetime(temp_data_load['date'])
                temp_data_load['location'] = location

                #Check if location already exists in df
                if complete_df.empty:
                    self.empty_load.append(location)
                    complete_df = temp_data_load
                elif ((complete_df['location'].isin([location]).any()) &
                      (column_name in complete_df.columns)):
                    self.update_attempts.append(location)
                    for date in list(temp_data_load['date']):
                        complete_df.loc[((complete_df['location']==location)
                                        & (complete_df['date']==date))
                                        ,column_name] = temp_data_load.loc[temp_data_load['date']==date
                                                       ,column_name].max()
                elif (complete_df['location'].isin([location]).any()):
                    self.merge_attempts.append(location)
                    complete_df=complete_df.merge(temp_data_load
                                                  , how='outer', on=['location'
                                                                     ,'date'])
                else:
                    self.append_attempts.append(location)
                    complete_df=complete_df.append(temp_data_load,sort=True)

        self.water_dataframe = complete_df.reset_index(drop=True)

    def convert_json_to_csv(self):
        """Convert returned json data to two csv files - station
        information and water data.


        """
        station_info_csv = open('station_information.csv','a')
        print('Creating csvs')
        json_data = self.response.json()
        self.station_dataframe=pandas.DataFrame()
        self.stations_returned=[]
        self.empty_load=[]
        self.update_attempts=[]
        self.merge_attempts=[]
        self.append_attempts=[]
        self.total_attempts=[]
        self.no_data=[]
        for data in json_data['value']['timeSeries']:
            location = data['sourceInfo']['siteCode'][0]['value']
            metric = data['variable']['variableName'].split(',')[0].lower()
            units= data['variable']['unit']['unitCode'].lower()
            statistic = data['variable']['options']['option'][0]['value'].lower()
            self.__create_station_dataframe(data)
            self.total_attempts.append(location)
            temp_data_load=pandas.DataFrame(data['values'][0]['value'])
            if 'qualifiers' in temp_data_load.columns:
                temp_data_load.drop(columns='qualifiers', inplace=True)


            if 'value' not in temp_data_load.columns:
                self.no_data.append(location)
                self.__raise_stations_with_no_data(location,metric)
                pass
            else:
                temp_data_load.rename(columns={'dateTime':'date'}
                                      ,inplace=True)
                temp_data_load['date'] = pandas.to_datetime(temp_data_load['date'])
                temp_data_load['date'] = temp_data_load['date'].dt.strftime("%Y-%m-%d")
                temp_data_load['station_number'] = location
                temp_data_load['metric']=metric
                temp_data_load['units']=units
                temp_data_load['statistic']=statistic

                #write the new temp_data_load to the csv
                temp_data_load[['station_number',
                                'date',
                                'metric',
                                'units',
                                'statistic',
                                'value']].to_csv('water_data.csv'
                                    ,sep=',',header=False
                                    ,index=False,mode='a')

        self.station_dataframe.drop_duplicates(inplace=True, ignore_index=True)
        self.station_dataframe[['site_number',
                                'site_name',
                                'latitude',
                                'longitude',
                                'site_type',
                                'watershed_code',
                                'state_code',
                                'county_code']].to_csv('station_information.csv'
                                                       ,sep=',',header=True
                                                       ,index=False,mode='a')


    def __create_station_dictionary(self,data):
        site_number = data['sourceInfo']['siteCode'][0]['value']
        site_name = data['sourceInfo']['siteName']
        geo_location = [data['sourceInfo']['geoLocation']['geogLocation']['latitude']
                         ,data['sourceInfo']['geoLocation']['geogLocation']['longitude']
                        ]
        site_type = data['sourceInfo']['siteProperty'][0]['value']

        self.station_dictionary.update({site_number:{'site_name':site_name,
                                                     'lat/long':geo_location,
                                                     'type':site_type}})

    def __create_station_dataframe(self,data):
        site_number = data['sourceInfo']['siteCode'][0]['value']
        site_name = data['sourceInfo']['siteName']
        latitude = data['sourceInfo']['geoLocation']['geogLocation']['latitude']
        longitude= data['sourceInfo']['geoLocation']['geogLocation']['longitude']

        for property in data['sourceInfo']['siteProperty']:
            if property['name']=='siteTypeCd':
                site_type = property['value']
            elif property['name'] == 'hucCd':
                watershed=property['value']
            elif property['name'] == 'stateCd':
                state_cd = property['value']
            elif property['name']=='countyCd':
                county_cd=property['value']
            else:
                #TODO: should probably raise a flag but continue
                pass
        station_temp_df=pandas.DataFrame.from_dict({'site_number':site_number,
                                 'site_name':site_name,
                                 'latitude':latitude,
                                 'longitude':longitude,
                                 'site_type':site_type,
                                 'watershed_code':watershed,
                                 'state_code':state_cd,
                                 'county_code':county_cd}
                                                   , orient='index').transpose()

        self.station_dataframe = self.station_dataframe.append(station_temp_df,
                                                               sort=True)

    def __create_major_filter(self):
        """A function to identify which major filter to use for the url
        construction. Only 1 major filter (station numbers, geographic
        boundary, us_state, us_counties) can be used to return station
        data and will be chosen by the previously listed heirarchy if
        multiple are given.
        """
        if self.station_numbers:

            self.major_filter = ("&sites="
                            +self.__converting_list_to_string(self.station_numbers)
                            )
            return
        elif self.geo_boundary_box:
            self.major_filter =("&bBox="
                           +self.__converting_list_to_string(self.geo_boundary_box)
                           )
            return
        elif self.us_state:

            self.major_filter = "&stateCd="+self.us_state
            return
        elif self.us_counties:

            self.major_filter = ("&countyCd="
                            +self.__coverting_list_to_string(self.us_counties)
                            )
            return
        else:
            raise Exception("""No major filter provided.
                            Please update with major filter.""")

    def __converting_list_to_string(self,list_to_convert):
        """A function to convert a list of numbers to a comma separated
        string.

        """
        converted_string=','.join(map(str, list_to_convert))

        return converted_string

    def __parse_start_end_dates(self):
        """ A function to parse the start dates and end dates.
        self.date_range should be in list format [start_date, end_date]
        """
        year_string_format = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}')

        self.start_date = "&startDT="+self.date_range[0]
        self.end_date = "&endDT="+self.date_range[-1]

    def __create_site_type_filter(self):
        #TODO: full name version of each filter based on
        #on https://help.waterdata.usgs.gov/site_tp_cd documentation

        if self.site_type:
            self.site_type_filter = '&siteType='+",".join(map(str,self.site_type))
        else:
            self.site_type_filter = ''

    def __raise_stations_with_no_data(self,station_number,metric):
        """If a station doesn't have data, add metric and station number
        to a dictionary.
        """

        try:
            self.no_data_stations[station_number].append(metric)
        except:
            try:
                self.no_data_stations[station_number]=[metric]
            except:
                self.no_data_stations={station_number:[metric]}

    def __create_metric_filter(self):
        """
        """
        self.metric_filter = '&parameterCd='+','.join(map(str,self.metric_codes))
